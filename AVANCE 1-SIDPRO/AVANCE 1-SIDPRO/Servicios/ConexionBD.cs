﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace AVANCE_1_SIDPRO.Servicios
{
    class ConexionBD
    {
       // private static MySqlConnection instancia = null;
        public static MySqlConnection Instancia
        {
            get
            {
                
                MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
                builder.Server = "localhost";
                builder.UserID = "root";
                builder.Password = "diana";
                builder.Database = "sidpro";
                return new MySqlConnection(builder.ToString());
            }
        }
    }
}
