﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using AVANCE_1_SIDPRO.Servicios;

namespace AVANCE_1_SIDPRO.Presentación
{
    public partial class login : Form
    {
        private MySqlConnection miConexion;

        private SeleccionZonas sz = null;
        public login()
        {
            InitializeComponent();
        }
        private void login_Load(object sender, EventArgs e)
        {
            miConexion = ConexionBD.Instancia;
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            string usu = txtUsuario.Text;
            string pas = txtPassword.Text;

            if (usu.Length == 0 || pas.Length == 0)
            {
                MessageBox.Show("Espacios en blanco!");
            }
            else
            {
                miConexion.Open();
                MySqlCommand cmd = miConexion.CreateCommand();
                cmd.CommandText = "SELECT contraseña FROM sidpro.cuentas WHERE usuario = '" + usu + "';";
                string aux = null;
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    aux = reader.GetString("contraseña");
                }
                if (pas == aux)
                {
                    if(sz == null)
                    {
                        sz = new SeleccionZonas();
                        sz.Show();
                    }else
                    {
                        sz.BringToFront();
                    }
                }
                else if(aux == null)
                {
                    MessageBox.Show("Usuario no encontrado!");
                }
                else
                {
                    MessageBox.Show("Contraseña Incorrecta!");
                }
                miConexion.Close();
            }
        }
    }
}
