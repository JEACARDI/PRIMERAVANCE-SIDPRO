﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms.Markers;
using AVANCE_1_SIDPRO.Repositorios;

namespace AVANCE_1_SIDPRO.Presentación
{
    public partial class SeleccionZonas : Form
    {
        double LatInicial = -12.049816;
        double LngInicial = -77.091781;

        public SeleccionZonas()
        {
            InitializeComponent();
        }
        private void SeleccionZonas_Load(object sender, EventArgs e)
        {
            gMapControl1.DragButton = MouseButtons.Left;
            gMapControl1.CanDragMap = true;
            gMapControl1.MapProvider = GMapProviders.GoogleMap;
            gMapControl1.Position = new PointLatLng(LatInicial, LngInicial);
            gMapControl1.MinZoom = 0;
            gMapControl1.MaxZoom = 24;
            gMapControl1.Zoom = 15;
            gMapControl1.AutoScroll = true;
        }

        private void btnDividir_Click(object sender, EventArgs e)
        {
            crear();
        }

        private void crear()
        {
            SI inteligencia = SI.Llamar;
            List<Zona> zonas = inteligencia.Zonas;
            foreach(Zona z in zonas)
            {
                GMapOverlay poligonos = new GMapOverlay(z.Nombre);
                poligonos.Polygons.Add(z.Poligono);
                gMapControl1.Overlays.Add(poligonos);
            }

            List<Paquete> Paquetes = inteligencia.paquetes;
            GMapOverlay markerOverlay = new GMapOverlay("Marcador");
            foreach (Paquete p in Paquetes)
            {
                GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(p.Coordenadas.lat, p.Coordenadas.lng), GMarkerGoogleType.green);
                markerOverlay.Markers.Add(marker);
            }

            gMapControl1.Overlays.Add(markerOverlay);

            gMapControl1.Zoom = gMapControl1.Zoom + 1;
            gMapControl1.Zoom = gMapControl1.Zoom - 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            crear();
        }
    }
}
