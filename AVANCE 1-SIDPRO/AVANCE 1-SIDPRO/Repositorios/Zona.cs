﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GMap.NET.WindowsForms;
using GMap.NET;

namespace AVANCE_1_SIDPRO.Repositorios
{
    class Zona
    {
        private double separacion = 0.001;
        private List<string> codPaquetes;
        private double latMax, latMin, lngMax, lngMin;
        private GMapPolygon poligono=null;
        private string nombre;

        public Zona(string nombre, List<string> codPaquetes, double latMax, double latMin, double lngMax, double lngMin)
        {
            this.nombre = nombre; this.codPaquetes = codPaquetes; this.latMax = latMax; this.latMin = latMin; this.lngMax = lngMax; this.lngMin = lngMin;
        }

        public List<string> CodPaquetes
        {
            get
            {
                return codPaquetes;
            }

            set
            {
                codPaquetes = value;
            }
        }

        public double LatMax
        {
            get
            {
                return latMax;
            }

            set
            {
                latMax = value;
            }
        }

        public double LatMin
        {
            get
            {
                return latMin;
            }

            set
            {
                latMin = value;
            }
        }

        public double LngMax
        {
            get
            {
                return lngMax;
            }

            set
            {
                lngMax = value;
            }
        }

        public double LngMin
        {
            get
            {
                return lngMin;
            }

            set
            {
                lngMin = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public GMapPolygon Poligono
        {
            get
            {
                //Se genera un cuadrante teniendo en cuenta las coordenadas máximas y mínimas
                if(poligono == null)
                {
                    List<PointLatLng> puntos = new List<PointLatLng>();
                    puntos.Add(new PointLatLng(latMin - separacion, lngMin - separacion));
                    puntos.Add(new PointLatLng(latMin - separacion, lngMax + separacion));
                    puntos.Add(new PointLatLng(latMax + separacion, lngMax + separacion));
                    puntos.Add(new PointLatLng(latMax + separacion, lngMin - separacion));
                    poligono = new GMapPolygon(puntos, nombre);
                }
                return poligono;
            }

            set
            {
                poligono = value;
            }
        }
    }
}
