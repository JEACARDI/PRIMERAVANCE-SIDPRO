﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using AVANCE_1_SIDPRO.Servicios;
using AVANCE_1_SIDPRO.Presentación;
using System.Windows.Forms;

namespace AVANCE_1_SIDPRO.Repositorios
{
    class SI
    {

        private static SI instancia = null; //Patrón singleton para tener una sola instancia
        private MySqlConnection miCon = null; // Conexion base de datos

        private double umbral = 0.0338521686070092; // Distancia máxima entre dos puntos para pertenecer a una zona
        private List<Paquete> Paquetes=null; //Lista de paquetes completa
        private List<List<Paquete>> paquetesDivididos = null; //Lista de paquetes divididos
        private List<bool> Seleccionado = null; //Lista de booleanos asociados a los paquetes para la selección de las zonas
        private List<Zona> zonas = null; //Lista de objetos Zona, que contiene el polígono de la zona

        private SI()
        {
        }

        public void dividirPorZonas()
        {
            //Se instancian los atributos
            Seleccionado = new List<bool>();
            paquetesDivididos = new List<List<Paquete>>();
            zonas = new List<Zona>();
            // Se inicializa todos los booleanos con falso
            for (int a = 0; a < Paquetes.Count; a++)
            {
                Seleccionado.Add(false);
            }
            int i = 0;
            // Primera zona nombrada Zona A
            char nZona = 'A';

            // Se selecciona un paquete central y se evalua la proximidad con el resto de paquetes
            foreach (Paquete pInicial in Paquetes)
            {
                //Si el paquete aún no ha sido seleccionado para pertenecer a una zona, se evalua
                if (!Seleccionado[i])
                {
                    // Zona auxiliar para agregar a la lista
                    List<Paquete> zona = new List<Paquete>();

                    // Lista de codigos y extremos para agregar a la zona auxiliar
                    List<string> codigos = new List<string>();
                    double latmax, latmin, lngmax, lngmin;

                    // Se selecciona el paquete y se agrega a la zona
                    Seleccionado[i] = true;
                    zona.Add(pInicial);

                    // Se agrega el código del paquete a la lista de códigos
                    codigos.Add(pInicial.Codigo);
                    // Se inicializan los extremos
                    latmax = pInicial.Coordenadas.lat; latmin = pInicial.Coordenadas.lat; lngmax = pInicial.Coordenadas.lng; lngmin = pInicial.Coordenadas.lng;
                    int j = 0;
                    foreach (Paquete p in Paquetes)
                    {
                        //Si el paquete no fue seleccionado, se evalua la proximidad con el primer paquete
                        if (!Seleccionado[j])
                        {
                            //Se calcula la distancia entre los dos puntos
                            double distancia = Math.Sqrt(Math.Pow(pInicial.Coordenadas.lat - p.Coordenadas.lat, 2) + Math.Pow(pInicial.Coordenadas.lng - p.Coordenadas.lng, 2));
                            //Si la distancia es menor que el umbral se almacena
                            if (distancia < umbral)
                            {
                                //Se añade el paquete a la zona y se selecciona
                                zona.Add(p);
                                Seleccionado[j] = true;
                                //Se agrega el código a la lista
                                codigos.Add(p.Codigo);
                                //Se evaluan los extremos
                                if (p.Coordenadas.lat < latmin)
                                    latmin = p.Coordenadas.lat;
                                if (p.Coordenadas.lat > latmax)
                                    latmax = p.Coordenadas.lat;
                                if (p.Coordenadas.lng < lngmin)
                                    lngmin = p.Coordenadas.lng;
                                if (p.Coordenadas.lng > lngmax)
                                    lngmax = p.Coordenadas.lng;
                            }
                        }
                        j++;
                    }
                    //Se agrega la nueva zona (Con paquetes) a la lista de paquetes divididos
                    PaquetesDivididos.Add(zona);
                    //Se agrega una nueva zona a la lista de zonas
                    zonas.Add(new Zona(nZona.ToString(), codigos, latmax, latmin, lngmax, lngmin));
                    nZona++;
                }
                i++;
            }

        }
        public List<List<Paquete>> PaquetesDivididos
        {
            get
            {
                //Si los paquetes aún no han sido divididos se ejecuta la función
                if (paquetesDivididos == null)
                    dividirPorZonas();
                return paquetesDivididos;
            }
        }

        internal List<Zona> Zonas
        {
            get
            {
                // Si las zonas aún no han sido creadas se ejecuta la función
                if (zonas == null)
                {
                    dividirPorZonas();
                }
                return zonas;
            }
        }

        internal List<Paquete> paquetes
        {
            get
            {
                //Si los paquetes aún no han sido recibidos se ejecuta la función
                if (Paquetes == null)
                    obtenerPaquetes();
                return Paquetes;
            }

            set
            {
                Paquetes = value;
            }
        }

        //Llamada a la única instancia de Sistema Inteligente(SI) mediante el patrón singleton
        public static SI Llamar
        {
            get
            {
                if (instancia == null)
                    instancia = new SI();
                return instancia;
            }
        }

        public void verificarHorarios()
        {
            miCon = ConexionBD.Instancia;// Se crea una base de datos
            miCon.Open();// Se abre la base de datos
            // Se crea un comando de selección
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT codigo,Nombres,Apellidos,Teléfono,Correo,Horario FROM sidpro.paquete";
            //Se crea un DataReader para almacenar la tabla y se ejecuta el comando
            MySqlDataReader reader = cmd.ExecuteReader();
            short horario;
            string nombres, apellidos, correo, telefono, codigo;
            //Se empieza a leer la tabla guardad en el DataREader
            while (reader.Read())
            {
                codigo = reader.GetString("codigo");
                nombres = reader.GetString("Nombres");
                apellidos = reader.GetString("Apellidos");
                telefono = reader.GetString("Teléfono");
                correo = reader.GetString("Correo");
                try
                {
                    //En el caso de que retorne NULL (Horario no escogido)
                    horario = reader.GetInt16("Horario");
                }
                catch (System.Data.SqlTypes.SqlNullValueException)
                {
                    //Se genera una instancia de seleccionar horario
                    Application.Run(new seleccionHorario(codigo, nombres, apellidos, telefono, correo));
                }
            }
            //Se cierra la base de datos
            miCon.Close();
        }

        private void obtenerPaquetes()
        {
            //Se instancia el atributo PAquete
            Paquetes = new List<Paquete>();
            miCon = ConexionBD.Instancia; //Se crea la conexión a la base de datos
            miCon.Open(); // Se aber la conexión a la base de datos
            //Se crea un comando de selección
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT * FROM sidpro.paquete";
            //Se crea un DataReader que almacenará los datos de los paquetes para su instanciación
            MySqlDataReader reader = cmd.ExecuteReader();
            //Se lee la tabla y se agregan paquetes a la lista
            while (reader.Read())
            {
                Paquetes.Add(new Paquete(reader.GetString("codigo"), new coord(reader.GetDouble("Lat"), reader.GetDouble("Long"))));
            }
            //Se cierra la base de datos
            miCon.Close();
        }
    }
}
