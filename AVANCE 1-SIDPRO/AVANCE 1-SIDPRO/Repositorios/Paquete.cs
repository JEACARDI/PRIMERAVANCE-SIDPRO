﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AVANCE_1_SIDPRO.Repositorios
{
    struct coord
    {
        public double lat;
        public double lng;
        public coord(double lat, double lng)
        {
            this.lat = lat; this.lng = lng;
        }
    }
    struct medidas
    {
        public float ancho;
        public float largo;
        public float alto;
    }
    class Paquete
    {
        private string codigo;
        private string nombres;
        private string apellidos;
        private string telefono;
        private string correo;
        private coord coordenadas;
        private bool fragil;
        private medidas medida;
        private string idRepartidor;

        public Paquete(string codigo, coord coordenadas)
        {
            this.codigo = codigo; this.coordenadas = coordenadas;
        }

        public string Codigo
        {
            get
            {
                return codigo;
            }

            set
            {
                codigo = value;
            }
        }

        public string Nombres
        {
            get
            {
                return nombres;
            }

            set
            {
                nombres = value;
            }
        }

        public string Apellidos
        {
            get
            {
                return apellidos;
            }

            set
            {
                apellidos = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public coord Coordenadas
        {
            get
            {
                return coordenadas;
            }

            set
            {
                coordenadas = value;
            }
        }

        public bool Fragil
        {
            get
            {
                return fragil;
            }

            set
            {
                fragil = value;
            }
        }

        public medidas Medida
        {
            get
            {
                return medida;
            }

            set
            {
                medida = value;
            }
        }

        public string IdRepartidor
        {
            get
            {
                return idRepartidor;
            }

            set
            {
                idRepartidor = value;
            }
        }
    }

}
